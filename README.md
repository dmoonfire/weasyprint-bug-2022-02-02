# WeasyPrint Layout Bug

This is the bug I'm currently having.

```shell
❯ weasyprint --version
WeasyPrint version 53.4
```

I'm using the NixOS unstable for this. Included is the flake for Nix that duplicates the environment I'm using for the test case.
